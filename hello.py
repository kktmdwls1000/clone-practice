def say_hello():
    animals = ['cat', 'dog', 'hamster']
    food = [
        'ramen',
        'ramyun',
        'spagetti', # trailing comma
        'udong',
    ]
    colors = [
        'red',
        'blue',
        'green',
        'yellow'  
   ]
    print('hello, world!')

if __name__=='__main__':
    say_hello()
